// Import required libraries
#include <Arduino.h>

// PINS
#define PIN_BOARD_LED 0
#define PIN_FSR A0
#define PIN_RGB_RED 12
#define PIN_RGB_GREEN 13
#define PIN_RGB_BLUE 14

// CONSTANTS
const int MS_TO_READ = 100;
const int MS_TO_CHECK = 1500;
const int CASING_DEVIATION = 250;
const int MS_TO_DRINK_TIME = 60000;
const int MS_TO_BLINK = 1000;

// Variables
int onBoardLedState = 0;
int blinkState = 0;
int fsrReading;
int fsrReadings[20];
int checkReading;
int placedReading;
bool placed = false;
uint32_t lastSample = 0;
uint32_t lastCheck = 0;
uint32_t lastPlacedChange = 0;
uint32_t lastBlink = 0;

void setup(void)
{
    // Start Serial
    Serial.begin(115200);

    // Init pins
    pinMode(PIN_BOARD_LED, OUTPUT);
    pinMode(PIN_RGB_RED, OUTPUT);
    pinMode(PIN_RGB_GREEN, OUTPUT);
    pinMode(PIN_RGB_BLUE, OUTPUT);

    setRgbLed(0, 0, 0);
}

void loop() {
    if(millis() - lastCheck > MS_TO_CHECK) {
      lastCheck = millis();

      int average = 0;

      for(int i = 0; i < MS_TO_CHECK / MS_TO_READ; i++) {
        average = average + fsrReadings[i];
      }

      average = average / (MS_TO_CHECK / MS_TO_READ);

      Serial.print("Average: ");
      Serial.println(average);

      int diff = abs(checkReading - average);

      if(checkReading < average) {
        if(checkReading - CASING_DEVIATION <= 100 && diff >= 50) {
//          digitalWrite(PIN_BOARD_LED, 0);

          placedReading = average;

          lastPlacedChange = millis();
          placed = true;
          Serial.println("Placed");
        }
      } else if(checkReading > average) {
        if(average - CASING_DEVIATION <= 100 && diff >= 50) {
//          digitalWrite(PIN_BOARD_LED, 1);

          lastPlacedChange = millis();
          placed = false;
          Serial.println("Removed");
        }
      }

      checkReading = average;
    }

    digitalWrite(PIN_BOARD_LED, !placed);

    float percent = (float) ((float) (millis() - lastPlacedChange) / (float) MS_TO_DRINK_TIME); 

    if(percent > 1) {
      percent = 1;

      if(millis() - lastBlink > MS_TO_BLINK) {
        lastBlink = millis();
        blinkState = blinkState <= 0 ? 1 : 0;

        setRgbLed(blinkState <= 0 ? 0 : 255, 0, 0);
      }
    } else if(placed) {
      int red = round(percent * 255);
      int green = 0;
      int blue = round((1 - percent) * 255);
      
      setRgbLed(red, green, blue);
    } else {
      setRgbLed(0, 0, 0);
    }
  
    if (millis() - lastSample > MS_TO_READ) { // Every 100ms:
      lastSample = millis();
      fsrReadings[(int) ((millis() - lastCheck) / MS_TO_READ)] = fsrReading;
      fsrReading = 0;
    }

    int reading = analogRead(PIN_FSR);
    
    if(reading > fsrReading) {
      fsrReading = reading;
    }
}

void setRgbLed(int red, int green, int blue) {
    analogWrite(PIN_RGB_RED, 4 * (255 - red));
    analogWrite(PIN_RGB_GREEN, 4 * (255 - green));
    analogWrite(PIN_RGB_BLUE, 4 * (255 - blue));
}

void flipOnBoardLedState() {
    onBoardLedState = onBoardLedState <= 0 ? 1 : 0;
    digitalWrite(PIN_BOARD_LED, onBoardLedState);
} 
